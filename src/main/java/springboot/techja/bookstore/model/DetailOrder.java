package springboot.techja.bookstore.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "detail_order")
public class DetailOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "detail_order_id")
    private int detailOrderId;

    @Column(name = "order_id")
    private int orderId;

    @Column(name = "book_id")
    private int bookId;

    @Column(name = "amount")
    private int amount;

    @Column(name = "total_price")
    private int totalPrice;
}
