package springboot.techja.bookstore.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "book")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "book_id")
    private int userId;

    @Column(name = "author_id")
    private int authorId;

    @Column(name = "topic_id")
    private int topicId;

    @Column(name = "book_name")
    private String bookName;

    @Column(name = "book_img_path")
    private String bookImgPath;

    @Column(name = "price")
    private int price;

    @Column(name = "book_amount")
    private int bookAmount;

    @Column(name = "book_description")
    private String bookDescription;
}
